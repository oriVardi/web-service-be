package oriOmry.ws;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {
	@RequestMapping(
			path="/usersCtrl/{id}", 
			method=RequestMethod.GET,
			
			// Accept=application/json
			// Accept=application/xml
//			produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
			produces= MediaType.APPLICATION_JSON_VALUE
			)
	public String  get (){
		System.out.println("appppp");
		return "{id:123,tt:wer}";
	}
}
